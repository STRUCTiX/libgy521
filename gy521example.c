#include <stdio.h>
#include <wiringPi.h>
#include "libgy521.h"


void printall(void) {
    printf("---------------------------------------------\n");
    printf("Accel: x=%f, y=%f, z=%f\n", getAccelerometerData('x', 1), getAccelerometerData('y', 1), getAccelerometerData('z', 1));
    printf("Accel ms2: x=%f, y=%f, z=%f\n", getAccelerometerData('x', 0), getAccelerometerData('y', 0), getAccelerometerData('z', 0));
    printf("Accel Range: raw: %i, int: %i\n", readAccelerometerRange(1), readAccelerometerRange(0));
    printf("Gyro: x=%f, y=%f, z=%f\n", getGyroscopeData('x'), getGyroscopeData('y'), getGyroscopeData('z'));
    printf("Gyro Range: raw: %i, int: %i\n", readGyroscopeRange(1), readGyroscopeRange(0));
    printf("Temp: %f\n", getTemp());
    printf("---------------------------------------------\n");
}

int main(void) {
    wiringPiSetup();
    
    setupgy521();
    printall();
    while (1) {
        //printf("AngleX = %f, AngleY = %f, AngleZ = %f, Temperature = %f\n", getAngleX(), getAngleY(), getAngleZ(), getTemp());
        //printf("Gyro: x=%i, y=%i, z=%i\n", getGyroscopeData('x'), getGyroscopeData('y'), getGyroscopeData('z'));
        printall();
        delay(500);
    }
}
