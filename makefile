all: lib test

lib: libgy521.o
	cc -shared -O3 -o libgy521.so libgy521.o -lm -lwiringPi

test: gy521example.o
	cc -o test -L. gy521example.o -lgy521 -lwiringPi
	
.PHONY: clean
clean:
	rm -r *.o
