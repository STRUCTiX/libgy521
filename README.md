# Library for the GY-521 Breakboard

## Compiling the library
This library depends on the wiringPi library. Make sure to install it first (`sudo apt-get install wiringpi` but often it's preinstalled on raspbian).

1. Compile the library by typing `make` in the projects root
2. Copy the `libgy521.so` file to `/usr/lib` and the `libgy521.h` header file to `/usr/include`
3. In case of an error run `ldconfig` to detect the new library
4. have fun :)

## Including the library
After you have compiled and copied the library properly just include it (`#include <libgy521.h>`) and compile with the flag `-lgy521`.

## Documentation
[MPU-6050 Datasheet](https://www.olimex.com/Products/Modules/Sensors/MOD-MPU6050/resources/RM-MPU-60xxA_rev_4.pdf)

[WiringPi I2C Library](http://wiringpi.com/reference/i2c-library/)
